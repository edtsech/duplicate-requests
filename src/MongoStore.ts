import { AnyError, MongoClient, Collection } from "mongodb";

export type RequestDocument = {
  id: string;
  emittedAt: Date;
};

interface Props {
  url: string;
  dbName: string;
  collectionName: string;
  ttlInMs: number;
}

export class MongoStore {
  public connected = false;
  public connectionError: AnyError | null = null;

  private connection: Promise<Collection<RequestDocument>>;
  private ttlInMs = 60000; // 1 minute TTL by default

  constructor(args: Props) {
    const { url, dbName, collectionName, ttlInMs } = args;
    this.ttlInMs = ttlInMs;

    this.connection = new Promise((resolve, reject) => {
      MongoClient.connect(url)
        .then((client: MongoClient) => {
          const db = client.db(dbName);
          const collection: Collection<RequestDocument> = db.collection<RequestDocument>(
            collectionName
          );

          const uniqueIndexPromise = collection.createIndex(
            { key: 1 },
            {
              unique: true,
              background: true
            }
          );

          uniqueIndexPromise
            .then(() => {
              collection
                .createIndex(
                  { expiresAt: 1 },
                  {
                    expireAfterSeconds: 0,
                    background: true
                  }
                )
                .then(() => resolve(collection))
                .catch(reject);
            })
            .catch(reject);
        })
        .catch(reject);
    });

    this.connection
      .then(_ => (this.connected = true))
      .catch((error: AnyError) => {
        this.connected = false;
        this.connectionError = error;
      });
  }

  /**
   * Generate the middleware function
   * @param id - Request id
   * @param onError - Optional on error callback
   * @return Returns "true" if no duplicate was found and document was inserted, otherwise "false"
   */
  async put(id: string, onError?: (e: Error) => void): Promise<boolean> {
    try {
      const connection = await this.connection;
      const bulk = connection.initializeUnorderedBulkOp();
      const currentTimeInMs = new Date().getTime();

      bulk
        .find({
          key: id,
          emittedAt: { $gte: new Date(currentTimeInMs - this.ttlInMs) }
        })
        .upsert()
        .updateOne({
          $set: {
            key: id
          },
          $setOnInsert: {
            emittedAt: new Date(currentTimeInMs),
            expiredAt: new Date(currentTimeInMs + this.ttlInMs)
          }
        });

      const result = await bulk.execute();

      return !result.nMatched;
    } catch (e) {
      onError && onError(e as any);
      return false;
    }
  }
}

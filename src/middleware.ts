import { NextFunction, Request, Response } from "express";
import { MongoStore } from "./MongoStore";

/**
 * Type for middleware function used by express
 * @param req - The request object
 * @param req - The response object
 * @param next - The next function
 * @return An empty Promise, to support async/await
 */
export type MiddlewareFunction = (
  req: Request,
  res: Response,
  next: NextFunction
) => Promise<void>;

/**
 * ErrorResponse
 * @param json - Javascript plain object to send to client if request is duplicated
 * @param statusCodes - Status code to send to the client if request is duplicated
 */
export type ErrorResponse = {
  json?: unknown;
  statusCode?: number;
};

/**
 * Key Builder
 */
export type KeyBuilder =
  /**
   * @param req - The request object
   * @return The id
   */

  (req: Request) => string;

/**
 * Middleware options passed at creation
 * @param expiration - TTL of requests
 * @param response - Error handling configuration
 * @param key - Way to get the id property
 * @param scope - Name of a collection
 * @param connectionUri - URI to connect to a redis or mongodb backend
 */
export type MiddlewareOptions = {
  expiration: number;
  response?: ErrorResponse;
  key: string | KeyBuilder;
  scope: string;
  connectionUri: string;
  dbName: string;
  onError?: (e: Error) => void;
};

/**
 * Generate the middleware function
 * @param options - Middleware options
 * @return The middleware function used by express
 */
export async function createMiddleware(
  options: MiddlewareOptions
): Promise<MiddlewareFunction> {
  const mongoStore = new MongoStore({
    url: options.connectionUri,
    dbName: options.dbName,
    collectionName: options.scope,
    ttlInMs: options.expiration || 60000
  });

  return async (req: Request, res: Response, next: NextFunction) => {
    // If options.property is a function, gets is return value
    const id: string =
      typeof options.key === "function"
        ? options.key(req)
        : String(req.query[options.key]);

    const inserted = await mongoStore.put(id, options.onError);

    if (inserted) {
      next();
    } else {
      const responseData: unknown = options?.response?.json ?? {
        error: "Too Many Requests"
      };
      const statusCode: number = options?.response?.statusCode ?? 429;

      res.status(statusCode).json(responseData);
    }
  };
}

import { createAgent } from "./helpers";

describe("Middleware", () => {
  it("should work with propertyName as a string", async () => {
    const agent = createAgent({ key: "id", prefix: "test" });

    await agent.get("/?id=test");
    const response = await agent.get("/?id=test");

    expect(response.status).toBe(429);
  });

  it("should work with propertyName as a function", async () => {
    const agent = createAgent({
      key: req => String(req.query.example),
      prefix: "test"
    });

    await agent.get("/?example=test");
    const response = await agent.get("/?example=test");

    expect(response.status).toBe(429);
  });

  it("should set statusCode", async () => {
    const statusCode = 404;

    const agent = createAgent({
      key: "id",
      response: { statusCode },
      prefix: "test"
    });

    await agent.get("/?id=test");
    const response = await agent.get("/?id=test");

    expect(response.status).toBe(statusCode);
  });

  it("should set json body", async () => {
    const json = { error: true };

    const agent = createAgent({
      key: "id",
      response: { json },
      prefix: "test"
    });

    await agent.get("/?id=test");
    const response = await agent.get("/?id=test");

    expect(response.body).toStrictEqual(json);
  });
});

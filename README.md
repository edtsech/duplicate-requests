# req-dedup-middleware

This middleware aims to avoid duplicated requests made by the client.

## Installation

```
yarn add req-dedup-middleware

# Storage
yarn add @keyv/redis
# or 
yarn add @keyv/mongo
```

## Usage :

```javascript
import duplicate from "req-dedup-middleware";
// or
const duplicate = require("req-dedup-middleware").default;

const express = require("express");
const app = express();

app.use(duplicate({
    expiration: 10000,
    key: "requestId",
    prefix: "root"
}));

app.get("/", (req, res) => res.end("Hey!"));

app.listen(8080, () => console.log("Listening!"));
```

### Options

```javascript
{
  expiration: 10000, /* Expiration time of the request in milliseconds */
  key: "id", /* Property which contains the id
                   * should be a string or a function 
                   * with a req paramater which returns a string
                   */
  prefix: "requests", // Prefix to group requests in storage
  response: {
    statusCode: 429, // The status code to send if request is duplicated
    json: {} // Javascript plain object to send if request is duplicated
  },
  connectionUri: "" // Leave empty to store object in memory, or use redis:// or mongodb://
}
```

### External storage

If you want to use an external storage (currently supported are Redis and MongoDB), you need to install one of the following package :

```bash
yarn add @keyv/mongo # TTL monitor runs every minute (not configurable) 
yarn add @keyv/redis # TTL process runs every second
```
